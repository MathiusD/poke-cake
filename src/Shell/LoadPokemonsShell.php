<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Http\Client;
use Cake\ORM\TableRegistry;

/**
 * LoadPokemons shell command.
 */
class LoadPokemonsShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        $parser->addOption('replace', ['short' => 'r', 'help' => 'Supplements Pre-Existing Pokemon ', 'default' => true, 'boolean' => true]);

        return $parser;
    }

    protected function getTypeId($type_name)
    {
        $query = TableRegistry::getTableLocator()->get('PokemonTypes')->find()->select(['name', 'id']);
        foreach ($query as $types) {
            if (strtoupper($types['name']) == strtoupper($type_name))
                return $types['id'];
        }
        return -1;
    }

    protected function PokemonTraitement($Stat, $value, $compt, $pokedexNumber, $replace)
    {
        $type_id = $this->getTypeId($Stat->getJson()['types'][0]['type']['name']);
        if ($type_id != -1)
        {
            $data_extract = [
                'name' => $value['name'],
                'pokedex_number' => $pokedexNumber,
                'health' => ($Stat->getJson()['stats'][5]['base_stat']),
                'attack' => $Stat->getJson()['stats'][4]['base_stat'],
                'defense' => $Stat->getJson()['stats'][3]['base_stat'],
                'speed' => $Stat->getJson()['stats'][0]['base_stat'],
                'type_id' => $type_id
            ];
            if ($replace == false)
            {
                $query = TableRegistry::getTableLocator()->get('Pokes')->query();
                $query->insert(array_keys($data_extract));
                $query->values($data_extract)->execute();
            }
            else
            {
                $query_ = TableRegistry::getTableLocator()->get('Pokes')->find();
                $query_->select(['id'])->where(['pokedex_number' => $pokedexNumber]);
                foreach ($query_ as $data)
                    $data_extract['id'] = $data['id'];
                $query = TableRegistry::getTableLocator()->get('Pokes');
                $poke = $query->get($data_extract['id']);
                $poke->name = $data_extract['name'];
                $poke->health = $data_extract['health'];
                $poke->attack = $data_extract['attack'];
                $poke->defense = $data_extract['defense'];
                $poke->speed = $data_extract['speed'];
                $poke->type_id = $data_extract['type_id'];
                $query->save($poke);
            }
            $compt++;
            $this->verbose("Pokemon(".$pokedexNumber."):".$value['name']."is Inserted");
        }
        else
        {
            $this->verbose("Pokemon(".$pokedexNumber."):".$value['name']."is Not Inserted (Type Not Found)");
        }
        return $compt;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $compt = 0;
        $http = new Client();
        $url = "https://pokeapi.co/api/v2/pokemon/?offset=0&limit=5000";
        $rep = $http->get($url);
        if(!empty($rep->getJson()))
        {
            $dataapi = $rep->getJson();
            foreach ($dataapi['results'] as $key => $value)
            {
                $pokedexNumber = (int)(explode('/', $value['url'])[6]);
                $url = $value['url'];
                $Stat = $http->get($url);
                $query = TableRegistry::getTableLocator()->get('Pokes')->find();
                $query->select(['pokedex_number'])->where(['pokedex_number' => $pokedexNumber]);
                if($query->count() == 0)
                {
                    $compt = $this->PokemonTraitement($Stat, $value, $compt, $pokedexNumber, false);
                }
                else
                {
                    if ($this->params['replace'] ==  false)
                    {
                        $this->verbose("Pokemon(".$pokedexNumber."):".$value['name']."is Not Inserted (Pokemon Already in Database)");
                    }
                    else
                    {
                        $compt = $this->PokemonTraitement($Stat, $value, $compt, $pokedexNumber, true);
                    }
                }
            }
        }
        else
        {
            $this->quiet("No Data Method Get In This Url");
        }
        $this->quiet("Pokemon inserted : " . $compt);
    }
}
