<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RatioType Entity
 *
 * @property int $id
 * @property int $attack_type_id
 * @property int $defend_type_id
 * @property float $ratio
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\PokemonType $pokemon_type
 * @property \App\Model\Entity\PokemonType $attack_pokemon_type
 * @property \App\Model\Entity\PokemonType $defend_pokemon_type
 */
class RatioType extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'attack_type_id' => true,
        'defend_type_id' => true,
        'ratio' => true,
        'created' => true,
        'modified' => true,
        'pokemon_type' => true,
        'attack_pokemon_type' => true,
        'defend_pokemon_type' => true
    ];
}
