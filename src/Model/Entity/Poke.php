<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Poke Entity
 *
 * @property int $id
 * @property string $name
 * @property int $pokedex_number
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $health
 * @property float $attack
 * @property float $defense
 * @property float $speed
 * @property int $type_id
 *
 * @property \App\Model\Entity\PokemonType $pokemon_type
 * @property \App\Model\Entity\Dresseur[] $dresseurs
 */
class Poke extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'pokedex_number' => true,
        'created' => true,
        'modified' => true,
        'health' => true,
        'attack' => true,
        'defense' => true,
        'speed' => true,
        'type_id' => true,
        'pokemon_type' => true,
        'dresseurs' => true
    ];
}
