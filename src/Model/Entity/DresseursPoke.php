<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DresseursPoke Entity
 *
 * @property int $id
 * @property int $poke_id
 * @property int $dresseur_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property bool $is_fav
 * @property string $name
 * @property int $lvl
 * @property int $exp_act
 * @property int $exp_max
 *
 * @property \App\Model\Entity\Poke $poke
 * @property \App\Model\Entity\Dresseur $dresseur
 */
class DresseursPoke extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'poke_id' => true,
        'dresseur_id' => true,
        'created' => true,
        'modified' => true,
        'is_fav' => true,
        'name' => true,
        'lvl' => true,
        'exp_act' => true,
        'exp_max' => true,
        'poke' => true,
        'dresseur' => true
    ];
}
