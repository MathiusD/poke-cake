<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Pokes Model
 *
 * @property \App\Model\Table\PokemonTypesTable&\Cake\ORM\Association\BelongsTo $PokemonTypes
 * @property \App\Model\Table\DresseursTable&\Cake\ORM\Association\BelongsToMany $Dresseurs
 *
 * @method \App\Model\Entity\Poke get($primaryKey, $options = [])
 * @method \App\Model\Entity\Poke newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Poke[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Poke|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Poke saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Poke patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Poke[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Poke findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PokesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('pokes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PokemonTypes', [
            'foreignKey' => 'type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Dresseurs', [
            'foreignKey' => 'poke_id',
            'targetForeignKey' => 'dresseur_id',
            'joinTable' => 'dresseurs_pokes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->integer('pokedex_number')
            ->requirePresence('pokedex_number', 'create')
            ->notEmptyString('pokedex_number');

        $validator
            ->integer('health')
            ->requirePresence('health', 'create')
            ->notEmptyString('health');

        $validator
            ->numeric('attack')
            ->requirePresence('attack', 'create')
            ->notEmptyString('attack');

        $validator
            ->numeric('defense')
            ->requirePresence('defense', 'create')
            ->notEmptyString('defense');

        $validator
            ->numeric('speed')
            ->requirePresence('speed', 'create')
            ->notEmptyString('speed');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['type_id'], 'PokemonTypes'));

        return $rules;
    }
}
