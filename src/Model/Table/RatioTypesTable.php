<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RatioTypes Model
 *
 * @property \App\Model\Table\PokemonTypesTable&\Cake\ORM\Association\BelongsTo $PokemonTypes
 * @property \App\Model\Table\PokemonTypesTable&\Cake\ORM\Association\BelongsTo $PokemonTypes
 *
 * @method \App\Model\Entity\RatioType get($primaryKey, $options = [])
 * @method \App\Model\Entity\RatioType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RatioType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RatioType|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RatioType saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RatioType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RatioType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RatioType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RatioTypesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ratio_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('PokemonTypes', [
            'foreignKey' => 'attack_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PokemonTypes', [
            'foreignKey' => 'defend_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('AttackPokemonTypes', [
            'className' => 'PokemonTypes',
            'foreignKey' => 'attack_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('DefendPokemonTypes', [
            'className' => 'PokemonTypes',
            'foreignKey' => 'defend_type_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('ratio')
            ->requirePresence('ratio', 'create')
            ->notEmptyString('ratio');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['attack_type_id'], 'PokemonTypes'));
        $rules->add($rules->existsIn(['defend_type_id'], 'PokemonTypes'));

        return $rules;
    }
}
