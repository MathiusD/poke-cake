<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DresseursPokes Model
 *
 * @property \App\Model\Table\PokesTable&\Cake\ORM\Association\BelongsTo $Pokes
 * @property \App\Model\Table\DresseursTable&\Cake\ORM\Association\BelongsTo $Dresseurs
 *
 * @method \App\Model\Entity\DresseursPoke get($primaryKey, $options = [])
 * @method \App\Model\Entity\DresseursPoke newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DresseursPoke[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DresseursPoke|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DresseursPoke saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DresseursPoke patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DresseursPoke[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DresseursPoke findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DresseursPokesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('dresseurs_pokes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Pokes', [
            'foreignKey' => 'poke_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Dresseurs', [
            'foreignKey' => 'dresseur_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('is_fav')
            ->notEmptyString('is_fav');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->integer('lvl')
            ->requirePresence('lvl', 'create')
            ->notEmptyString('lvl');

        $validator
            ->integer('exp_act')
            ->requirePresence('exp_act', 'create')
            ->notEmptyString('exp_act');

        $validator
            ->integer('exp_max')
            ->requirePresence('exp_max', 'create')
            ->notEmptyString('exp_max');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['poke_id'], 'Pokes'));
        $rules->add($rules->existsIn(['dresseur_id'], 'Dresseurs'));

        return $rules;
    }
}
