<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * RatioTypes Controller
 *
 * @property \App\Model\Table\RatioTypesTable $RatioTypes
 *
 * @method \App\Model\Entity\RatioType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RatioTypesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PokemonTypes', 'AttackPokemonTypes', 'DefendPokemonTypes']
        ];
        $ratioTypes = $this->paginate($this->RatioTypes);

        $this->set(compact('ratioTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Ratio Type id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ratioType = $this->RatioTypes->get($id, [
            'contain' => ['PokemonTypes', 'AttackPokemonTypes', 'DefendPokemonTypes']
        ]);

        $this->set('ratioType', $ratioType);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ratio Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ratioType = $this->RatioTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $ratioType = $this->RatioTypes->patchEntity($ratioType, $this->request->getData());
            if ($this->RatioTypes->save($ratioType)) {
                $this->Flash->success(__('The ratio type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The ratio type could not be saved. Please, try again.'));
        }
        $pokemonTypes = $this->RatioTypes->PokemonTypes->find('list', ['limit' => 200]);
        $this->set(compact('ratioType', 'pokemonTypes'));
    }
}
