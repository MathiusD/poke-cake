<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DresseursPokes Controller
 *
 * @property \App\Model\Table\DresseursPokesTable $DresseursPokes
 *
 * @method \App\Model\Entity\DresseursPoke[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DresseursPokesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Pokes', 'Dresseurs']
        ];
        $dresseursPokes = $this->paginate($this->DresseursPokes);

        $this->set(compact('dresseursPokes'));
    }

    /**
     * View method
     *
     * @param string|null $id Dresseurs Poke id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dresseursPoke = $this->DresseursPokes->get($id, [
            'contain' => ['Pokes', 'Dresseurs']
        ]);

        $this->set('dresseursPoke', $dresseursPoke);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dresseursPoke = $this->DresseursPokes->newEntity();
        if ($this->request->is('post')) {
            $dresseursPoke = $this->DresseursPokes->patchEntity($dresseursPoke, $this->request->getData());
            if ($this->DresseursPokes->save($dresseursPoke)) {
                $this->Flash->success(__('The dresseurs poke has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dresseurs poke could not be saved. Please, try again.'));
        }
        $pokes = $this->DresseursPokes->Pokes->find('list', ['limit' => 200]);
        $dresseurs = $this->DresseursPokes->Dresseurs->find('list', ['limit' => 200]);
        $this->set(compact('dresseursPoke', 'pokes', 'dresseurs'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dresseurs Poke id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dresseursPoke = $this->DresseursPokes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dresseursPoke = $this->DresseursPokes->patchEntity($dresseursPoke, $this->request->getData());
            if ($this->DresseursPokes->save($dresseursPoke)) {
                $this->Flash->success(__('The dresseurs poke has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dresseurs poke could not be saved. Please, try again.'));
        }
        $pokes = $this->DresseursPokes->Pokes->find('list', ['limit' => 200]);
        $dresseurs = $this->DresseursPokes->Dresseurs->find('list', ['limit' => 200]);
        $this->set(compact('dresseursPoke', 'pokes', 'dresseurs'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dresseurs Poke id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dresseursPoke = $this->DresseursPokes->get($id);
        if ($this->DresseursPokes->delete($dresseursPoke)) {
            $this->Flash->success(__('The dresseurs poke has been deleted.'));
        } else {
            $this->Flash->error(__('The dresseurs poke could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
