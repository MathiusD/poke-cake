<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight[]|\Cake\Collection\CollectionInterface $fights
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Fight'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Trainers List'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Trainer'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="fights index large-9 medium-8 columns content">
    <h3><?= __('Fights') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="computer" scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('first_dresseur_id') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('second_dresseur_id') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('winner_dresseur_id') ?></th>
                <th id="computer" scope="col" class="actions"><?= __('Actions') ?></th>
                <th id="portable" scope="col">Fights</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($fights as $fight): ?>
            <tr>
                <td id="computer"><?= $this->Number->format($fight->id) ?></td>
                <td id="computer"><?= $fight->has('first_dresseur') ? $this->Html->link($fight->first_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->first_dresseur->id]) : '' ?></td>
                <td id="computer"><?= $fight->has('second_dresseur') ? $this->Html->link($fight->second_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->second_dresseur->id]) : '' ?></td>
                <td id="computer"><?= $fight->has('winner_dresseur') ? $this->Html->link($fight->winner_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->winner_dresseur->id]) : '' ?></td>
                <td class="actions" id ="computer">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $fight->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fight->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fight->id)]) ?>
                </td>
                <td id="portable"><?= $this->Html->link(__('Fight between'.$fight->second_dresseur->first_name.'and'.$fight->second_dresseur->first_name), ['action' => 'view', $fight->id])?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
