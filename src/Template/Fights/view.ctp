<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fight $fight
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(__('Delete Fight'), ['action' => 'delete', $fight->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fight->id)]) ?> </li>
        <li><?= $this->Html->link(__('Fights List'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fight'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Trainers List'), ['controller' => 'Dresseurs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Trainer'), ['controller' => 'Dresseurs', 'action' => 'add']) ?> </li>    </ul>
</nav>
<div class="fights view large-9 medium-8 columns content">
    <h3>Fight between <?= $fight->has('first_dresseur') ? $this->Html->link($fight->first_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->first_dresseur->id]) : '' ?> and <?= $fight->has('second_dresseur') ? $this->Html->link($fight->second_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->second_dresseur->id]) : '' ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('First Trainer') ?></th>
            <td><?= $fight->has('first_dresseur') ? $this->Html->link($fight->first_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->first_dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Second Trainer') ?></th>
            <td><?= $fight->has('second_dresseur') ? $this->Html->link($fight->second_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->second_dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Winner Trainer') ?></th>
            <td><?= $fight->has('winner_dresseur') ? $this->Html->link($fight->winner_dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $fight->winner_dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($fight->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($fight->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($fight->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Fight Log') ?></h4>
        <textarea class="log-fight" autocomplete="off" cols="30" placeholder="Fight_Log" wrap="soft" disabled="true"><?= h($fight->fight_log); ?></textarea>
    </div>
</div>
