<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke $poke
 */
use Cake\View\Helper\HtmlHelper;
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Pokemon'), ['action' => 'edit', $poke->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Pokemon'), ['action' => 'delete', $poke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poke->id)]) ?> </li>
        <li><?= $this->Html->link(__('Pokemons List'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pokemon'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Types List'), ['controller' => 'PokemonTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Type'), ['controller' => 'PokemonTypes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Trainer List'), ['controller' => 'Dresseurs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Trainer'), ['controller' => 'Dresseurs', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pokes view large-9 medium-8 columns content">
    <h3><?= h($poke->name) ?></h3>
    <?php
        $img = $this->Html->image('pokes/'.$this->Number->format($poke->pokedex_number).'.png', ['alt' => $poke->name]);
        $file_path_temp = explode('/', explode('"', explode("=", explode(" ", $img)[1])[1])[1]);
        $file_path = "";
        for($i = 2; $i < count($file_path_temp); $i++)
            $file_path = $file_path.'/'.$file_path_temp[$i];
        $file_path = getcwd().$file_path;
    ?>
    <?php if(file_exists($file_path)): ?>
        <?= $img ?>
    <?php else: ?>
        <img id="frontSpritesPokemon" class="img-pokemon"/>
    <?php endif?>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($poke->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Pokemon Type') ?></th>
            <td><?= $poke->has('pokemon_type') ? $this->Html->link($poke->pokemon_type->name, ['controller' => 'PokemonTypes', 'action' => 'view', $poke->pokemon_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('DEX#') ?></th>
            <td><?= $this->Number->format($poke->pokedex_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('HP') ?></th>
            <td><?= $this->Number->format($poke->health) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Attack') ?></th>
            <td><?= $this->Number->format($poke->attack) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Defense') ?></th>
            <td><?= $this->Number->format($poke->defense) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Speed') ?></th>
            <td><?= $this->Number->format($poke->speed) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($poke->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($poke->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Dresseurs') ?></h4>
        <?php if (!empty($poke->dresseurs)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($poke->dresseurs as $dresseurs): ?>
            <tr>
                <td><?= h($dresseurs->first_name) ?></td>
                <td><?= h($dresseurs->last_name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Dresseurs', 'action' => 'view', $dresseurs->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Dresseurs', 'action' => 'edit', $dresseurs->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Dresseurs', 'action' => 'delete', $dresseurs->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseurs->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
<script>
$(document).ready(function(){
    return getSprite(<?php echo $poke->pokedex_number?>)
});
function getSprite(PokeNumber){
    $.ajax({
        url: "http://pokeapi.co/api/v2/pokemon/"+ PokeNumber +"/",
        type: "GET",
        dataType: "json",
        success: function(data) {
            $("#frontSpritesPokemon").attr("src", data.sprites.front_default)
        }
    });
}
</script>