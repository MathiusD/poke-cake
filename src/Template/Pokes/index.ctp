<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke[]|\Cake\Collection\CollectionInterface $pokes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Pokemon'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Types List'), ['controller' => 'PokemonTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Trainers List'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Trainer'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pokes index large-9 medium-8 columns content">
    <h3><?= __('Pokes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="computer" scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th id="portable" scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('pokedex_number') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('health') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('attack') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('defense') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('speed') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('type_id') ?></th> <!-- must replace by a SQL querry to get the Type's name instead -->
                <th id="computer" scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pokes as $poke): ?>
            <tr>
                <td id="computer"><?= h($poke->name) ?></td>
                <td id="computer"><?= $this->Number->format($poke->pokedex_number) ?></td>
                <td id="computer"><?= $this->Number->format($poke->health) ?></td>
                <td id="computer"><?= $this->Number->format($poke->attack) ?></td>
                <td id="computer"><?= $this->Number->format($poke->defense) ?></td>
                <td id="computer"><?= $this->Number->format($poke->speed) ?></td>
                <td id="computer"><?= $poke->has('pokemon_type') ? $this->Html->link($poke->pokemon_type->name, ['controller' => 'PokemonTypes', 'action' => 'view', $poke->pokemon_type->id]) : '' ?></td>
                <td id="computer"class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $poke->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $poke->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $poke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $poke->id)]) ?>
                </td>
                <td id="portable"><?= $this->Html->link(__($poke->name), ['action' => 'view', $poke->id]) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
