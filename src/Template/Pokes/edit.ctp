<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Poke $poke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $poke->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $poke->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Pokemons'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Types'), ['controller' => 'PokemonTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Type'), ['controller' => 'PokemonTypes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Trainers'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Trainer'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="pokes form large-9 medium-8 columns content">
    <?= $this->Form->create($poke) ?>
    <fieldset>
        <legend><?= __('Edit Poke') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('pokedex_number');
            echo $this->Form->control('health');
            echo $this->Form->control('attack');
            echo $this->Form->control('defense');
            echo $this->Form->control('speed');
            echo $this->Form->control('type_id', ['options' => $pokemonTypes]);
            echo $this->Form->control('dresseurs._ids', ['options' => $dresseurs]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
