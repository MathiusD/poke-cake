<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PokemonType[]|\Cake\Collection\CollectionInterface $pokemonTypes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
    </ul>
</nav>
<div class="pokemonTypes index large-9 medium-8 columns content">
    <h3><?= __('Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="computer" scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th id="portable" scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th id="computer" scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pokemonTypes as $pokemonType): ?>
            <tr>
                <td id="computer"><?= h($pokemonType->name) ?></td>
                <td class="actions" id="computer">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $pokemonType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pokemonType->id]) ?>
                </td>
                <td id="portable"><?= $this->Html->link(__($pokemonType->name), ['action' => 'view', $pokemonType->id]) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
