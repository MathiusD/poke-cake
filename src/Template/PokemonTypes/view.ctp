<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PokemonType $pokemonType
 * @var int[] $type_association
 */
use Cake\ORM\TableRegistry;

    
function getTypeRatioTable($type_id)
{
    $typetable = array();
    $query = TableRegistry::getTableLocator()->get('RatioTypes')->find()->select(['defend_type_id', 'ratio'])->where(['attack_type_id =' => $type_id]);
    foreach ($query as $data) {
        $typetable[$data['defend_type_id']] = $data['ratio'];
    }
    return $typetable;
}

function getTypeRatio($attacker, $defenser)
{
    return TableRegistry::getTableLocator()->get('RatioTypes')->find()->select(['ratio'])->where(['attack_type_id =' => $attacker])->where(['defend_type_id =' => $defenser])->first()['ratio'];
}

function getTypeRatioId($attacker, $defenser)
{
    return TableRegistry::getTableLocator()->get('RatioTypes')->find()->select(['id'])->where(['attack_type_id =' => $attacker])->where(['defend_type_id =' => $defenser])->first()['id'];
}

function getNameType($type_id)
{
    return TableRegistry::getTableLocator()->get('PokemonTypes')->find()->select(['name'])->where(['id =' => $type_id])->first()['name'];
}

function getNbType()
{
    $compt = 0;
    $query = TableRegistry::getTableLocator()->get('PokemonTypes')->find();
    foreach ($query as $types) {
        $compt++;
    }
    return $compt;
}

$nb_type = getNbType();
$type_association = getTypeRatioTable($pokemonType->id);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Type'), ['action' => 'edit', $pokemonType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Type'), ['action' => 'delete', $pokemonType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pokemonType->id)]) ?> </li>
        <li><?= $this->Html->link(__('Types List'), ['action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="pokemonTypes view large-9 medium-8 columns content">
    <h3><?= h($pokemonType->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($pokemonType->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('#') ?></th>
            <td><?= $this->Number->format($pokemonType->id) ?></td>
        </tr>
    </table>
    <div class="related" id="computer">
        <h4><?= __('Related Types Association') ?></h4>
        <?php if ($type_association != null):?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= $this->Paginator->sort('defend_type_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ratio') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php for($i = 1; $i <= $nb_type; $i++): ?>
            <?php if(array_key_exists($i, $type_association)): ?>
            <tr>
                <td><?= $this->Html->link(getNameType($i), ['controller' => 'PokemonTypes', 'action' => 'view', $i])?></td>
                <td><?= $this->Number->format($type_association[$i]) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'RatioTypes', 'action' => 'view', getTypeRatioId($pokemonType->id, $i)]) ?>
                </td>
            </tr>
            <?php else: ?>
            <tr>
                <td><?= $this->Html->link(getNameType($i), ['controller' => 'PokemonTypes', 'action' => 'view', $i])?></td>
                <td><?= $this->Number->format(1) ?></td>
            </tr>
            <?php endif; ?>
            <?php endfor; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
