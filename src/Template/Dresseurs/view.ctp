<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur $dresseur
 */

use Cake\ORM\TableRegistry;

function isFav($trainer_id, $poke_id)
{
    return TableRegistry::getTableLocator()->get('DresseursPokes')->find()->select(['is_fav'])->where(['poke_id =' => $poke_id])->where(['dresseur_id =' => $trainer_id])->first()['is_fav'];
}

function Id_Capture($trainer_id, $poke_id)
{
    return TableRegistry::getTableLocator()->get('DresseursPokes')->find()->select(['id'])->where(['poke_id =' => $poke_id])->where(['dresseur_id =' => $trainer_id])->first()['id'];
}

function getNameType($type_id)
{
    return TableRegistry::getTableLocator()->get('PokemonTypes')->find()->select(['name'])->where(['id =' => $type_id])->first()['name'];
}
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Trainer'), ['action' => 'edit', $dresseur->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Trainer'), ['action' => 'delete', $dresseur->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseur->id)]) ?> </li>
        <li><?= $this->Html->link(__('Trainers List'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Trainer'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Pokemons List'), ['controller' => 'Pokes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pokemon'), ['controller' => 'Pokes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dresseurs view large-9 medium-8 columns content">
    <h3><?= h($dresseur->first_name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($dresseur->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($dresseur->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($dresseur->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($dresseur->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Pokemons Captures') ?></h4>
        <?php if (!empty($dresseur->pokes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Pokedex Number') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Health') ?></th>
                <th scope="col"><?= __('Attack') ?></th>
                <th scope="col"><?= __('Defense') ?></th>
                <th scope="col"><?= __('Speed') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Favori') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($dresseur->pokes as $pokes): ?>
            <tr>
                <td><?= h($pokes->id) ?></td>
                <td><?= h($pokes->name) ?></td>
                <td><?= h($pokes->pokedex_number) ?></td>
                <td><?= h($pokes->created) ?></td>
                <td><?= h($pokes->modified) ?></td>
                <td><?= h($pokes->health) ?></td>
                <td><?= h($pokes->attack) ?></td>
                <td><?= h($pokes->defense) ?></td>
                <td><?= h($pokes->speed) ?></td>
                <td><?= $this->Html->link(getNameType($pokes->type_id), ['controller' => 'PokemonTypes', 'action' => 'view', $pokes->type_id])?></td>
                <td><?= h(IsFav($dresseur->id, $pokes->id)) ?></td>
                <td class="actions">
                    <p><?= $this->Html->link(__('View(Pokedex)'), ['controller' => 'Pokes', 'action' => 'view', $pokes->id]) ?></p>
                    <p><?= $this->Html->link(__('View(Pokemon)'), ['controller' => 'DresseursPokes', 'action' => 'view', Id_Capture($dresseur->id, $pokes->id)]) ?></p>
                </td>
            </tr>
            <?php endforeach; ?>
            <?= $this->Html->link(__('Add Capture'), ['action' => 'add']) ?>
        </table>
        <?php endif; ?>
    </div>
</div>
