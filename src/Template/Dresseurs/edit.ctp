<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur $dresseur
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $dresseur->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $dresseur->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Trainers List'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Pokemons List'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pokemon'), ['controller' => 'Pokes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dresseurs form large-9 medium-8 columns content">
    <?= $this->Form->create($dresseur) ?>
    <fieldset>
        <legend><?= __('Edit Trainer') ?></legend>
        <?php
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('pokes._ids', ['options' => $pokes]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
