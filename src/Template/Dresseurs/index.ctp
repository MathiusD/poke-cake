<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Dresseur[]|\Cake\Collection\CollectionInterface $dresseurs
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Trainer'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Pokemons List'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pokemon'), ['controller' => 'Pokes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dresseurs index large-9 medium-8 columns content">
    <h3><?= __('Dresseurs') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="computer" scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                <th id="portable" scope="col"><?= $this->Paginator->sort('first_name') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                <th id="computer" scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dresseurs as $dresseur): ?>
            <tr>
                <td id="computer"><?= h($dresseur->first_name) ?></td>
                <td id="computer"><?= h($dresseur->last_name) ?></td>
                <td id="computer" class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dresseur->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseur->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseur->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseur->id)]) ?>
                </td>
                <td id="portable"><?= $this->Html->link(__($dresseur->first_name), ['action' => 'view', $dresseur->id]) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
