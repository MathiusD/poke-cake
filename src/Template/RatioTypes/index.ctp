<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RatioType[]|\Cake\Collection\CollectionInterface $ratioTypes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Types List'), ['controller' => 'PokemonTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Type'), ['controller' => 'PokemonTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ratioTypes index large-9 medium-8 columns content">
    <h3><?= __('Ratio Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="computer" scope="col"><?= $this->Paginator->sort('attack_type_id') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('defend_type_id') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('ratio') ?></th>
                <th id="computer" scope="col" class="actions"><?= __('Actions') ?></th>
                <th id="portable" scope="col">Associations</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($ratioTypes as $ratioType): ?>
            <tr>
                <td id="computer"><?= $ratioType->has('attack_pokemon_type') ? $this->Html->link($ratioType->attack_pokemon_type->name, ['controller' => 'PokemonTypes', 'action' => 'view', $ratioType->attack_pokemon_type->id]) : '' ?></td>
                <td id="computer"><?= $ratioType->has('defend_pokemon_type') ? $this->Html->link($ratioType->defend_pokemon_type->name, ['controller' => 'PokemonTypes', 'action' => 'view', $ratioType->defend_pokemon_type->id]) : '' ?></td>
                <td id="computer"><?= $this->Number->format($ratioType->ratio) ?></td>
                <td id="computer" class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $ratioType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $ratioType->id]) ?>
                </td>
                <td id="portable"><?= $this->Html->link(__('Association of Types '.$ratioType->attack_pokemon_type->name.' and '.$ratioType->defend_pokemon_type->name), ['action' => 'view', $ratioType->id]) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
