<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RatioType $ratioType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Type Association'), ['action' => 'edit', $ratioType->id]) ?> </li>
        <li><?= $this->Html->link(__('Types Association List'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Types List'), ['controller' => 'PokemonTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Type'), ['controller' => 'PokemonTypes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ratioTypes view large-9 medium-8 columns content">
    <h3>Association of Types <?= $ratioType->has('attack_pokemon_type') ? $this->Html->link($ratioType->attack_pokemon_type->name, ['controller' => 'PokemonTypes', 'action' => 'view', $ratioType->attack_pokemon_type->id]) : '' ?> and <?= $ratioType->has('defend_pokemon_type') ? $this->Html->link($ratioType->defend_pokemon_type->name, ['controller' => 'PokemonTypes', 'action' => 'view', $ratioType->defend_pokemon_type->id]) : '' ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Attack Type') ?></th>
            <td><?= $ratioType->has('attack_pokemon_type') ? $this->Html->link($ratioType->attack_pokemon_type->name, ['controller' => 'PokemonTypes', 'action' => 'view', $ratioType->attack_pokemon_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Defend Type') ?></th>
            <td><?= $ratioType->has('defend_pokemon_type') ? $this->Html->link($ratioType->defend_pokemon_type->name, ['controller' => 'PokemonTypes', 'action' => 'view', $ratioType->defend_pokemon_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ratio') ?></th>
            <td><?= $this->Number->format($ratioType->ratio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($ratioType->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($ratioType->modified) ?></td>
        </tr>
    </table>
</div>
