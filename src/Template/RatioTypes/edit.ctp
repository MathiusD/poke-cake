<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RatioType $ratioType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Types Association List'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Types List'), ['controller' => 'PokemonTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Type'), ['controller' => 'PokemonTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="ratioTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($ratioType) ?>
    <fieldset>
        <legend><?= __('Edit Type Association') ?></legend>
        <?php
            echo $this->Form->control('attack_type_id', ['options' => $pokemonTypes]);
            echo $this->Form->control('defend_type_id', ['options' => $pokemonTypes]);
            echo $this->Form->control('ratio');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
