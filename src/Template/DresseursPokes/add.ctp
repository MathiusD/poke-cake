<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseursPoke $dresseursPoke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Captures List'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Pokemons'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pokemon'), ['controller' => 'Pokes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Trainers List'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Trainer'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dresseursPokes form large-9 medium-8 columns content">
    <?= $this->Form->create($dresseursPoke) ?>
    <fieldset>
        <legend><?= __('Add Capture') ?></legend>
        <?php
            echo $this->Form->control('poke_id', ['options' => $pokes]);
            echo $this->Form->control('dresseur_id', ['options' => $dresseurs]);
            echo $this->Form->control('is_fav');
            echo $this->Form->control('name');
            echo $this->Form->control('lvl');
            echo $this->Form->control('exp_act');
            echo $this->Form->control('exp_max');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
