<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseursPoke[]|\Cake\Collection\CollectionInterface $dresseursPokes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Capture'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Pokemons List'), ['controller' => 'Pokes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Pokemon'), ['controller' => 'Pokes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('Trainers List'), ['controller' => 'Dresseurs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Trainer'), ['controller' => 'Dresseurs', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="dresseursPokes index large-9 medium-8 columns content">
    <h3><?= __('Captures') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th id="computer" scope="col"><?= $this->Paginator->sort('poke_id') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('dresseur_id') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('is_fav') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('lvl') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('exp_act') ?></th>
                <th id="computer" scope="col"><?= $this->Paginator->sort('exp_max') ?></th>
                <th id="computer" scope="col" class="actions"><?= __('Actions') ?></th>
                <th id="portable" scope="col">Capture</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($dresseursPokes as $dresseursPoke): ?>
            <tr>
                <td id="computer"><?= $dresseursPoke->has('poke') ? $this->Html->link($dresseursPoke->poke->name, ['controller' => 'Pokes', 'action' => 'view', $dresseursPoke->poke->id]) : '' ?></td>
                <td id="computer"><?= $dresseursPoke->has('dresseur') ? $this->Html->link($dresseursPoke->dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $dresseursPoke->dresseur->id]) : '' ?></td>
                <td id="computer"><?= h($dresseursPoke->is_fav) ?></td>
                <td id="computer"><?= h($dresseursPoke->name) ?></td>
                <td id="computer"><?= $this->Number->format($dresseursPoke->lvl) ?></td>
                <td id="computer"><?= $this->Number->format($dresseursPoke->exp_act) ?></td>
                <td id="computer"><?= $this->Number->format($dresseursPoke->exp_max) ?></td>
                <td id="computer" class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $dresseursPoke->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $dresseursPoke->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $dresseursPoke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseursPoke->id)]) ?>
                </td>
                <td id="portable"><?= $this->Html->link(__($dresseursPoke->dresseur->first_name."'s".$dresseursPoke->poke->name), ['action' => 'view', $dresseursPoke->id]) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
