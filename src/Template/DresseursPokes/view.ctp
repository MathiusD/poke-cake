<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DresseursPoke $dresseursPoke
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Capture'), ['action' => 'edit', $dresseursPoke->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Capture'), ['action' => 'delete', $dresseursPoke->id], ['confirm' => __('Are you sure you want to delete # {0}?', $dresseursPoke->id)]) ?> </li>
        <li><?= $this->Html->link(__('Captures List'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Capture'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Pokemons List'), ['controller' => 'Pokes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Pokemon'), ['controller' => 'Pokes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Trainers List'), ['controller' => 'Dresseurs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Trianer'), ['controller' => 'Dresseurs', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="dresseursPokes view large-9 medium-8 columns content">
    <h3><?= $dresseursPoke->has('dresseur') ? $this->Html->link($dresseursPoke->dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $dresseursPoke->dresseur->id]) : '' ?>'s <?= $dresseursPoke->has('poke') ? $this->Html->link($dresseursPoke->poke->name, ['controller' => 'Pokes', 'action' => 'view', $dresseursPoke->poke->id]) : '' ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Pokemon') ?></th>
            <td><?= $dresseursPoke->has('poke') ? $this->Html->link($dresseursPoke->poke->name, ['controller' => 'Pokes', 'action' => 'view', $dresseursPoke->poke->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Dresseur') ?></th>
            <td><?= $dresseursPoke->has('dresseur') ? $this->Html->link($dresseursPoke->dresseur->first_name, ['controller' => 'Dresseurs', 'action' => 'view', $dresseursPoke->dresseur->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($dresseursPoke->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($dresseursPoke->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lvl') ?></th>
            <td><?= $this->Number->format($dresseursPoke->lvl) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Exp Act') ?></th>
            <td><?= $this->Number->format($dresseursPoke->exp_act) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Exp Max') ?></th>
            <td><?= $this->Number->format($dresseursPoke->exp_max) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($dresseursPoke->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($dresseursPoke->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Fav') ?></th>
            <td><?= $dresseursPoke->is_fav ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
