<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PokemonTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PokemonTypesTable Test Case
 */
class PokemonTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PokemonTypesTable
     */
    public $PokemonTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PokemonTypes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PokemonTypes') ? [] : ['className' => PokemonTypesTable::class];
        $this->PokemonTypes = TableRegistry::getTableLocator()->get('PokemonTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PokemonTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
