<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FightsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FightsTable Test Case
 */
class FightsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FightsTable
     */
    public $Fights;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Fights',
        'app.Dresseurs',
        'app.FirstDresseurs',
        'app.SecondDresseurs',
        'app.WinnerDresseurs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Fights') ? [] : ['className' => FightsTable::class];
        $this->Fights = TableRegistry::getTableLocator()->get('Fights', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fights);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
