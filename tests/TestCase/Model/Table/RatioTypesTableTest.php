<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RatioTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RatioTypesTable Test Case
 */
class RatioTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RatioTypesTable
     */
    public $RatioTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RatioTypes',
        'app.PokemonTypes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RatioTypes') ? [] : ['className' => RatioTypesTable::class];
        $this->RatioTypes = TableRegistry::getTableLocator()->get('RatioTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RatioTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
