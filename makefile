default_target: setup

re_pull:
	@git stash
	@git pull

re_bake: setup
	@bin/cake bake all dresseurs
	@bin/cake bake all pokes
	@bin/cake bake all dresseurs_pokes
	@bin/cake bake all fights
	@bin/cake bake all pokemon_types
	@bin/cake bake all ratio_types
	@bin/cake bake shell LoadPokemons

setup: install
	@bin/cake migrations migrate
	@bin/cake LoadPokemons -v -r

install:
	@composer install
	@chmod +x bin/cake