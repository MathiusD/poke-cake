<?php
use Migrations\AbstractMigration;

class AddTypesToPokes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('pokes');
        $table->addColumn('type_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addForeignKey('type_id', 'pokemon_types', 'id');
        $table->update();
    }
}
