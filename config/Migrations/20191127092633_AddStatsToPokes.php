<?php
use Migrations\AbstractMigration;

class AddStatsToPokes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('pokes');
        $table->addColumn('health', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('attack', 'float', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('defense', 'float', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('speed', 'float', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->update();
    }
}
