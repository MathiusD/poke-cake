<?php
use Migrations\AbstractMigration;

class AddPropertiesToDresseurPokes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('dresseurs_pokes');
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('lvl', 'integer',[
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('exp_act', 'integer',[
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('exp_max', 'integer',[
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->update();
    }
}
