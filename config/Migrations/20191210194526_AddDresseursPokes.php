<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class AddDresseursPokes extends AbstractMigration
{
    protected function getPokeName($id_poke)
    {
        $query = TableRegistry::getTableLocator()->get('Pokes')->find()->where(['id =' => $id_poke]);
        foreach ($query as $data) {
            return $data['name'];
        }
    }
    /**
     * Migrate Up.
     */
    public function up()
    {
        // inserting multiple rows
        $rows = extract_csv('config/Migrations/csv/pokemontrainer.csv');
        for($i = 0; $i < count($rows); $i++)
            $rows[$i]['name'] = $this->getPokeName($rows[$i]['poke_id']);
        $this->table('dresseurs_pokes')->insert($rows)->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        
    }
}
