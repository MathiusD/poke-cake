<?php
use Migrations\AbstractMigration;

class CreateDresseursPokes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('dresseurs_pokes');
        $table->addColumn('poke_id', 'integer',[
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('dresseur_id', 'integer',[
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'timestamp', [
            'default' => 'CURRENT_TIMESTAMP',
            'null' => false,
        ]);
        $table->addColumn('modified', 'timestamp', [
            'default' => 'CURRENT_TIMESTAMP',
            'null' => false,
        ]);
        $table->addForeignKey('poke_id', 'pokes', 'id');
        $table->addForeignKey('dresseur_id', 'dresseurs', 'id');
        $table->create();
    }
}
