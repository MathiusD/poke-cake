<?php
use Migrations\AbstractMigration;
include "csv/extract_csv.php";

class AddPokemonTypes extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        // inserting multiple rows
        $rows = extract_csv('config/Migrations/csv/Types.csv');

        $this->table('pokemon_types')->insert($rows)->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        
    }
}
