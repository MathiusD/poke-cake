<?php
function extract_csv($File)
{
    $arrResult  = array();
    $handle     = fopen($File, "r");
    $entete = false;
    $entete_data = array();
    if(empty($handle) === false) {
        while(($data = fgetcsv($handle, 1000, ",")) !== FALSE){
            $data_r = explode(':',$data[0]);
            if ($entete == false)
            {
                $entete_data = $data_r;
                $entete = true;
            }
            else
            {
                $temp = array();
                for ($i = 0; $i < count($data_r); $i++)
                {
                    if ($data_r[$i] != 'true' && $data_r[$i] != 'false')
                    {
                        $temp[$entete_data[$i]] = $data_r[$i];
                    }
                    else
                    {
                        if ($data_r[$i] != 'true')
                        {
                            $temp[$entete_data[$i]] = false;
                        }
                        else
                        {
                            $temp[$entete_data[$i]] = true;
                        }
                    }
                }
                $arrResult[] = $temp;
            }
        }
        fclose($handle);
    }
    return $arrResult;
}
?>