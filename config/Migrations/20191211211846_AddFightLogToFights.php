<?php
use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class AddFightLogToFights extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('fights');
        $table->addColumn('fight_log', 'text', [
            'limit' => MysqlAdapter::TEXT_LONG,
            'default' => null,
            'null' => false,
        ]);
        $table->update();
    }
}
