<?php
use Migrations\AbstractMigration;

class AddPokes extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        // inserting multiple rows
        $rows = extract_csv('config/Migrations/csv/pokedex.csv');

        $this->table('pokes')->insert($rows)->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        
    }
}
